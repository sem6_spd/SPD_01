#include "functions.hpp"

int jobs_time(std::vector<t_job>& jobs) {
	int max_C=0;
	int i;
	int n = jobs.size();

	// wyznaczanie czasow C i S
	// pierwszy element
	jobs.at(0).S = jobs.at(0).r;
	jobs.at(0).C = jobs.at(0).S + jobs.at(0).p;

	// pozostale elementy
	for (i = 1; i < n; i++) {
		jobs.at(i).S = std::max(jobs.at(i).r, jobs.at(i - 1).C);
		jobs.at(i).C = jobs.at(i).S + jobs.at(i).p;
		//std::cout << "S: " << jobs.at(i).S << " C: " << jobs.at(i).C << std::endl;
	}

	// wyznaczanie maksymalnego czasu - czasu wykonania uszeregowanych zadan
	for (i = 0; i < n; i++) {
		//std::cout << "max_C = " << max_C << " job = " << jobs.at(i).C + jobs.at(i).q << std::endl;
		max_C = std::max(max_C, jobs.at(i).C + jobs.at(i).q);
	}

	return max_C;
}

int jobs_time(std::vector<t_job>& jobs, const int& j) {
	int max_C=0;
	int i;
	int n = ((j <= jobs.size() && j >= 0) ? j : jobs.size());

	// wyznaczanie czasow C i S
	// pierwszy element
	jobs.at(0).S = jobs.at(0).r;
	jobs.at(0).C = jobs.at(0).S + jobs.at(0).p;

	// pozostale elementy
	for (i = 1; i < n; i++) {
		jobs.at(i).S = std::max(jobs.at(i).r, jobs.at(i - 1).C);
		jobs.at(i).C = jobs.at(i).S + jobs.at(i).p;
		//std::cout << "S: " << jobs.at(i).S << " C: " << jobs.at(i).C << std::endl;
	}

	// wyznaczanie maksymalnego czasu - czasu wykonania uszeregowanych zadan
	for (i = 0; i < n; i++) {
		//std::cout << "max_C = " << max_C << " job = " << jobs.at(i).C + jobs.at(i).q << std::endl;
		max_C = std::max(max_C, jobs.at(i).C);
	}

	return max_C;
}

std::vector<t_job>* load_data(const std::string& filename) {
	int n,	// liczba zadan
		v;	// ilosc parametrow na zadanie (niepotrzebne)
	
	std::ifstream file;				// plik z danymi
	std::vector<t_job>* job_list_default = new std::vector<t_job>();

	// ladowanie danych z pliku
	file.open(filename.c_str(), std::ios::in);
	if (file.is_open()) {
		file >> n;
		file >> v;

		for (int i = 0; i < n; i++) {
			t_job new_job;
			file >> new_job.r >> new_job.p >> new_job.q;
			job_list_default->push_back(new_job);
		}
	}
	else exit(-2);
	file.close();

	return job_list_default;
}

int jobs_Rsort(std::vector<t_job>& jobs) {
	std::sort(jobs.begin(), jobs.end());
	return jobs_time(jobs);
}

int jobs_2opt(std::vector<t_job>& jobs) {
	// algorytm 2-opt
	int temp_overall_time = 0,	// czas wykonywania - tymczasowe dla porownywania
		overall_time = 0;		// czas wykonywania sie wszystkich zadan

	int i, j,					// indeksy
		n = jobs.size();		// liczba zadan

	std::vector<t_job> job_list2(jobs);
	overall_time = jobs_time(job_list2);

	// iteruje po wszystkich elementach
	for (i = 0; i < n - 1; i++) {
		// kazdy kolejny element  odnosi sie do i-tego
		for (j = i + 1; j < n; j++) {
			std::swap(job_list2.at(i), job_list2.at(j));
			temp_overall_time = jobs_time(job_list2);

			// jesli lepszy czas
			if (temp_overall_time < overall_time) {
				overall_time = temp_overall_time;
				i = 0;
				j = 1;
			}
			// jesli gorszy lub taki sam czas
			else {
				std::swap(job_list2.at(i), job_list2.at(j));
			}
		}
	}

	return overall_time;
}

int jobs_Rsort_2opt(std::vector<t_job> &jobs) {
	jobs_Rsort(jobs);
	return jobs_2opt(jobs);
}

int jobs_schrage(std::vector<t_job>& jobs) {

	std::priority_queue<t_job, std::vector<t_job>, compare_jobs_r> N(jobs.begin(), jobs.end());
	std::priority_queue<t_job, std::vector<t_job>, compare_jobs_q> G;

	std::vector<t_job> sorted_jobs;

	int t = N.top().r;
	
	G.push(N.top());
	N.pop();

	sorted_jobs.push_back(G.top());
	t += G.top().p;
	G.pop();
	
	while (!N.empty() || !G.empty()) {
		while (!N.empty() && t >= N.top().r) {
			G.push(N.top());
			N.pop();
		}

		if (G.empty()) {
			G.push(N.top());
			t = N.top().r;
			N.pop();

		}

		sorted_jobs.push_back(G.top());
		t += G.top().p;
		G.pop();

	}

	return jobs_time(sorted_jobs);
}

int jobs_schrage_prmt(std::vector<t_job>& jobs) {

	// kolejka. prior. zadan przygotowywanych wg r rosnaco
	std::priority_queue<t_job, std::vector<t_job>, compare_jobs_r> N(jobs.begin(), jobs.end());

	// kolejka. prior. zadan gotowych wg q malejaco
	std::priority_queue<t_job, std::vector<t_job>, compare_jobs_q> G;	

	std::vector<t_job> sorted_jobs;

	int t = N.top().r;
	
	G.push(N.top());
	N.pop();

	sorted_jobs.push_back(G.top());
	t += G.top().p;
	G.pop();
	
	while (!N.empty() || !G.empty()) {
		while (!N.empty() && t >= N.top().r) {
			G.push(N.top());
			N.pop();
		}

		if (G.empty()) {
			G.push(N.top());
			t = N.top().r;
			N.pop();
		}
		
		if (!sorted_jobs.empty() && G.top().q > sorted_jobs.back().q) {

			t_job job_left = sorted_jobs.back();
			job_left.p = t - G.top().r;
			t -= job_left.p;

			G.push(job_left);
			sorted_jobs.back().p -= job_left.p;
			sorted_jobs.back().q = 0;
		}

			sorted_jobs.push_back(G.top());
			t += G.top().p;
			G.pop();
	}

	return jobs_time(sorted_jobs);
}

int jobs_schrage_modify(std::vector<t_job>* jobs) {

	std::priority_queue<t_job, std::vector<t_job>, compare_jobs_r> N(jobs->begin(), jobs->end());
	std::priority_queue<t_job, std::vector<t_job>, compare_jobs_q> G;

	std::vector<t_job> sorted_jobs;

	int t = N.top().r;
	
	G.push(N.top());
	N.pop();

	sorted_jobs.push_back(G.top());
	t += G.top().p;
	G.pop();
	
	while (!N.empty() || !G.empty()) {
		while (!N.empty() && t >= N.top().r) {
			G.push(N.top());
			N.pop();
		}

		if (G.empty()) {
			G.push(N.top());
			t = N.top().r;
			N.pop();

		}

		sorted_jobs.push_back(G.top());
		t += G.top().p;
		G.pop();

	}

	int time = jobs_time(sorted_jobs);
	jobs->swap(sorted_jobs);

	return time;
}

int jobs_schrage_prmt_modify(std::vector<t_job>* jobs) {

	// kolejka. prior. zadan przygotowywanych wg r rosnaco
	std::priority_queue<t_job, std::vector<t_job>, compare_jobs_r> N(jobs->begin(), jobs->end());

	// kolejka. prior. zadan gotowych wg q malejaco
	std::priority_queue<t_job, std::vector<t_job>, compare_jobs_q> G;	

	std::vector<t_job> sorted_jobs;

	int t = N.top().r;
	
	G.push(N.top());
	N.pop();

	sorted_jobs.push_back(G.top());
	t += G.top().p;
	G.pop();
	
	while (!N.empty() || !G.empty()) {
		while (!N.empty() && t >= N.top().r) {
			G.push(N.top());
			N.pop();
		}

		if (G.empty()) {
			G.push(N.top());
			t = N.top().r;
			N.pop();
		}
		
		if (!sorted_jobs.empty() && G.top().q > sorted_jobs.back().q) {

			t_job job_left = sorted_jobs.back();
			job_left.p = t - G.top().r;
			t -= job_left.p;

			G.push(job_left);
			sorted_jobs.back().p -= job_left.p;
			sorted_jobs.back().q = 0;
		}

			sorted_jobs.push_back(G.top());
			t += G.top().p;
			G.pop();
	}
	int time = jobs_time(sorted_jobs);
	jobs->swap(sorted_jobs);

	return time;
}

int jobs_carlier(std::vector<t_job>& jobs, const int& upper_bound) {

	std::vector<t_job> list(jobs.begin(), jobs.end());
	std::vector<t_job> list_opt;

	int UB = upper_bound;
	int U = jobs_schrage_modify(&list);

	int n = jobs.size();
	int a=-1,
		b=-1,
		c=-1;

	int i;

	int r_p,
		p_p,
		q_p;

	if (U < UB) {
		UB = U;
		std::copy(list.begin(), list.end(), list_opt.begin());
	}


	for (i = 0; i < n; i++) {
		if (U == jobs_time(list, i) + list.at(i).q) {
			b = i;
		}
	}

	if (b == -1) exit(b);

	for (i = 0; i < n; i++) {
		int sum = 0,
			k;

		if (i <= b) {
			for (k = i; k <= b; k++) {
				sum += list.at(k).p;
			}
		}
		
		if (U == list.at(i).r + sum + list.at(i).q) {
			a = i;
			break;
		}
	}

	if (a == -1) exit(a);

	for (i = a; i <= b; i++) {
		if (list.at(i).q < list.at(b).q) {
			c = i;
		}
	}

	if (c == -1) return U;
	
	r_p = q_p = std::numeric_limits<int>::max();
	p_p = 0;

	for (i = c+1; i <= b; i++) {
		r_p = std::min(r_p, list.at(i).r);
		q_p = std::min(q_p, list.at(i).q);
		p_p += list.at(i).p;
	}

	// TODO: od pkt 6. w dol

}